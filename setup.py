from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='fsf.vfp',
      version=version,
      description="Various for Plone",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='FSF webmaster',
      author_email='webmaster@fsf.org',
      url='',
      license='GPLv3 or later',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['fsf'],
      include_package_data=True,
      zip_safe=True,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
