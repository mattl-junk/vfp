# /usr/local/Plone/zeocluster/parts/instance/Extensions/sign_tal_petition.py

def tal_check_group(group_num, form):
    if group_num in form['mailing-lists']:
        return "1"
    return ""

def post_tal_petition_form(form):
    import socket, urllib2, urllib
    # custom_46 is Listenership.
    # custom_47 is Public Radio Membership.
    # Groups: 40 is the petition, 25 is FSS, 11 is PlayFreedom Activists,
    # Profile 35 is for creating people through the petition
    data = [('first_name', form['first_name']),
            ('last_name', form['last_name']),
            ('email-Primary', form['email-Primary']),
            ('custom_46', form['custom_46']),
            ('custom_47', form['custom_47']),
            ('postURL', ''),
            ('cancelURL', ''),
            ('_qf_default', 'Edit:cancel'),
            ('_qf_Edit_next', 'Save'),
            ('add_to_group', '40')]
    for group_num in '11', '25':
        if group_num in form['mailing-lists']:
            data.append(('add_to_group', group_num))
    data = urllib.urlencode(data)

    old_timeout = socket.getdefaulttimeout()
    socket.setdefaulttimeout(60 * 5)  # Wait 5 minutes for CiviCRM.
    try:
        f = urllib2.urlopen("https://crm.fsf.org/civicrm/profile/create&gid=35&reset=1", data)
        f.close()
    finally:
        socket.setdefaulttimeout(old_timeout)

# if __name__ == '__main__':
#     import sys
#     if (len(sys.argv) > 1) and (sys.argv[1] == 'debug'):
#         form = {'first_name': 'Sam',
#                 'last_name': 'Seaborn',
#                 'email-Primary': 'sseaborn@canonical.org',
#                 'custom_46': 'Would-Be Listener',
#                 'custom_47': '1',
#                 # 'posturl': '',
#                 # 'cancelurl': "/civicrm/profile?reset=1&amp;gid=35",
#                 # 'qf_default': 'Edit:cancel',
#                 # 'qf_edit_next': 'Save',
#                 'mailing-lists': []}
#         print "Posting...",
#         post_tal_petition_form(form)
#         print "done."
