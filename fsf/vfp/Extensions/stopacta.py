# /usr/local/Plone/zeocluster/parts/instance/Extensions/stopacta.py

def post_acta_form(form):
    import urllib2, urllib
    # Plone insists on sending True, civicrm wants 1.
    if form['is_opt_out'] == True:
        form['is_opt_out'] = "1"
    else:
        form['is_opt_out'] = "0"
    data = urllib.urlencode([('first_name', form['first_name']),
                            ('last_name', form['last_name']),
                            ('email-Primary', form['email-Primary']),
                            ('postURL', form['posturl']),
                            ('cancelURL', form['cancelurl']),
                            ('group[4]', form['group-4']),
                            ('_qf_default', form['qf_default']),
                            ('_qf_Edit_next', form['qf_edit_next']),
                            ('is_opt_out', form['is_opt_out']),])

                            
    f = urllib2.urlopen("https://crm.fsf.org/civicrm/profile/create&gid=7&reset=1", data)
