# /usr/local/Plone/zeocluster/parts/instance/Extensions/sign_uefi_petition.py

def uefi_check_group(group_num, form):
    if group_num in form['mailing-lists']:
        return "1"
    return ""

def post_uefi_petition_form(form):
    import socket, urllib2, urllib
    # Groups: 58 is the Secure Boot signers group
    # Profile 37 (gid=37) is for creating people through this form
    data = [('first_name', form['first_name']),
            ('last_name', form['last_name']),
            ('email-Primary', form['email-primary']),
            ('postURL', ''),
            ('cancelURL', ''),
            ('_qf_default', 'Edit:cancel'),
            ('_qf_Edit_next', 'Save'),
            ('group[58]', '1')]
    data = urllib.urlencode(data)
    old_timeout = socket.getdefaulttimeout()
    socket.setdefaulttimeout(60 * 5)  # Wait 5 minutes for CiviCRM.
    try:
        url = "https://crm.fsf.org/civicrm/profile/create&gid=37&reset=1"
        f = urllib2.urlopen(url, data)
        f.close()
    finally:
        socket.setdefaulttimeout(old_timeout)

# if __name__ == '__main__':
#     import sys
#     if (len(sys.argv) > 1) and (sys.argv[1] == 'debug'):
#         form = {'first_name': 'Joshua',
#                 'last_name': 'Gay',
#                 'email-Primary': 'jgay@fsf.org',
#                 'mailing-lists': []}
#         print "Posting...",
#         post_uefi_petition_form(form)
#         print "done."

